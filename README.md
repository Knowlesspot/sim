# SIM, or Sample Input Mapper
This program is a modern corpus based concatenative synthesizer.

The original idea was for creative realtime sampling, however the possibilities are vast. In future we want to be able to map songs, soundscapes, drum samples and more to create a useful VST.

## TODO:
 - Write mapper

## Long term goals:
 - Finish proof of concept in python with librosa / FluCoMa / PyAudioAnalysis / paura
 - Look into Essentia, Gist, FluCoMa for Nim rewrite
 - Write a realtime version in Nim
 - Create cross platform GUI
 - Make it a VST
 - Write documentation

 We are inspired by:
  - The infamous [sCrAmBlEd?HaCkZ!](https://www.youtube.com/watch?v=eRlhKaxcKpA)
  - The work of Diemo Schwarz on [CatRT](http://ismm.ircam.fr/catart/)
  - Rodrigo Constanzo's [C-C-Combine](http://rodrigoconstanzo.com/combine/)