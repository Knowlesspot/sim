import sys, os, csv
import numpy as np
import librosa as lr
import soundfile as sf
from presets import Preset
from sklearn.preprocessing import StandardScaler
import umap, umap.plot

CURRENTPATH = sys.path[0] + "\\FilesForSim"
SAMPLERATE = 22050
FRAMELENGTH = 4096
HOPLENGTH = 4096
PCMTYPE = "PCM_16"

librosa = Preset(lr)
librosa["sr"] = SAMPLERATE
librosa["n_fft"] = FRAMELENGTH
librosa["hop_length"] = HOPLENGTH
librosa["center"] = False

class Analyser:
  def __init__(self, fileName):
    filePath = CURRENTPATH + '\\' + fileName
    savePath = CURRENTPATH + '\\' + os.path.splitext(fileName)[0]
    if not os.path.exists(savePath):
      os.mkdir(savePath)
    blocks = self.analyse(filePath, savePath, store=False)
    #self.store(fileName, blocks)

  def analyse(self, filePath, savePath, store=True):
    filePath, sampleRate = self.resample(filePath)
    stream = lr.core.stream(filePath, block_length=1, fill_value=0, frame_length=FRAMELENGTH, hop_length=HOPLENGTH)
    blocks = []
    for i, block in enumerate(stream):
      writePath = savePath + '\\' + str(i) + ".wav"
      if store:
        sf.write(writePath, block, sampleRate, subtype=PCMTYPE)
      features = self.featureExtraction(block)
      features.insert(0, writePath)
      blocks.append(features)
    return blocks

  def resample(self, filePath):
    savePath = filePath
    sampleRate = lr.core.get_samplerate(filePath)
    if sampleRate != SAMPLERATE:
      savePath = os.path.splitext(filePath)[0] + "(Resampled).wav"
      if os.path.isfile(savePath) == False:
        resampled, sampleRate = lr.core.load(filePath, sr=SAMPLERATE)
        sf.write(savePath, resampled, sampleRate, subtype=PCMTYPE)
    return savePath, sampleRate

  def featureExtraction(self, block):
    chromaStft = lr.feature.chroma_stft(y=block, norm=np.inf, tuning=None, n_chroma=12)
    chromaCqt = lr.feature.chroma_cqt(y=block, fmin=None, norm=np.inf, threshold=0.0, n_chroma=12, n_octaves=7, bins_per_octave=24, cqt_mode='full')
    chromaCens = lr.feature.chroma_cens(y=block, fmin=None, n_chroma=12, n_octaves=7, bins_per_octave=24, cqt_mode='full', norm=2)
    mels = lr.feature.melspectrogram(y=block, power=2.0)
    mfcc = lr.feature.mfcc(y=block, n_mfcc=20, dct_type=2, norm='ortho', lifter=0)[0]
    rms = lr.feature.rms(y=block)[0][0]
    specCentroid = lr.feature.spectral_centroid(y=block)[0]
    specBandwidth = lr.feature.spectral_bandwidth(y=block, freq=None, centroid=None, norm=True, p=2)[0]
    specContrast = lr.feature.spectral_contrast(y=block, freq=None, fmin=200.0, n_bands=6, quantile=0.02, linear=False)
    specFlatness = lr.feature.spectral_flatness(y=block, amin=1e-10, power=2.0)[0]
    specRolloff = lr.feature.spectral_rolloff(y=block, freq=None, roll_percent=0.85)[0]
    polyFeatures = lr.feature.poly_features(y=block, order=1, freq=None)
    tonnetz = lr.feature.tonnetz(y=None, chroma=chromaCqt)
    zeroXRate = lr.feature.zero_crossing_rate(y=block)[0]
    pitches, magnitudes = lr.core.piptrack(y=block, threshold=0.1)
    onsetStrength =  lr.onset.onset_strength(y=block, lag=1, max_size=1, ref=None, detrend=False, feature=None, aggregate=None)
    return [chromaStft, chromaCqt, chromaCens, mels, mfcc, rms, specCentroid, specBandwidth, specContrast, specFlatness, specRolloff, polyFeatures, tonnetz, zeroXRate, pitches, magnitudes, onsetStrength]

  def store(self, fileName, data):
    fileName = os.path.splitext(fileName)[0]
    filePath = CURRENTPATH + '\\' + fileName + ".csv" 
    with open(filePath, 'ab') as file:
      #headers = ("BlockPath", "chromaStft", "chromaCqt", "chromaCens", "mels", "mfcc", "rms", "specCentroid", "specBandwidth", "specContrast", "specFlatness", "specRolloff", "polyFeatures", "tonnetz", "zeroXRate", "pitches", "magnitudes", "onsetStrength")
      #formats = ['%s', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e', '%.18e']
      for item in data:
        print(item)
        print(len(item))
        #np.savetxt(file, np.asarray(item), fmt=formats, delimiter=',', newline='\n')

class Plot:
  def __init__(self, fileName):
    filePath = CURRENTPATH + '\\' + fileName
    paths = []
    features = []
    with open(filePath, 'r', newline='') as file: 
      reader = csv.reader(file, delimiter=',')
      header = next(reader)
      for row in reader:
        paths.append(row[0])
        for item in row[1:]:
          item = np.fromstring(item)
        features.append(row[1:])
    #scaledData = StandardScaler().fit_transform(features)
    #reducer = umap.UMAP()
    #embedding = reducer.fit_transform(scaledData)

