import strformat, tables
import easywave

var wr = parseWaveFile("Sampled.wav", readRegions = true)

echo fmt"Endianness: {wr.endianness}"
echo fmt"Format:     {wr.format}"
echo fmt"Samplerate: {wr.sampleRate}"
echo fmt"Channels:   {wr.numChannels}"

# for ci in wr.chunks:
#   echo ci

# if wr.regions.len > 0:
#   for id, r in wr.regions.pairs:
#     echo fmt"id: {id}, {r}"

# var numBytes = wr.dataSize
# echo fmt"Sample data size: {numBytes} bytes"

# # File pointer is now at the start of the sample data